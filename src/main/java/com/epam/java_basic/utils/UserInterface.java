package com.epam.java_basic.utils;

import java.util.Scanner;

public final class UserInterface {

    private static final Scanner SCANNER = new Scanner(System.in);

    private UserInterface() {

    }

    public static int askNumber(String message) {
        System.out.println(message);
        int result = SCANNER.nextInt();
        SCANNER.nextLine();
        return result;
    }

    public static String askLine(String message) {
        System.out.println(message);
        return SCANNER.nextLine();
    }

    public static String askWord(String message) {
        System.out.println(message);
        String result = SCANNER.next();
        SCANNER.nextLine();
        return result;
    }

    public static void print(String message) {
        System.out.println(message);
    }

    public static void print(Number integer) {
        System.out.println(integer);
    }

    public static void printArray(int[] array) {
        for (Object element : array) {
            System.out.print(element + "\t");
        }
        System.out.println();
    }

}
